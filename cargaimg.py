#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Feb  4 14:49:32 2020

@author: Andrea
"""
# Imports PIL module  
from PIL import Image 
import matplotlib.pyplot as plt
import numpy as np
import cv2 

# open method used to open different extension image file

im00 = Image.open(r"/Users/Domo3/Desktop/MAESTRIA 2019/SEGUNDO CUATRIMESTRE/deni/flor.jpg")
im01 =Image.open(r"/Users/Domo3/Desktop/MAESTRIA 2019/SEGUNDO CUATRIMESTRE/deni/flor2.jpg")
 
plt.figure(1)
plt.imshow(im00)
plt.figure(2)
plt.imshow(im01)

im = im00.convert('LA')
im1 =im01.convert('LA')

plt.figure(3)
plt.imshow(im)
plt.figure(4)
plt.imshow(im1)

im2=np.asarray(im)
im3=np.asarray(im1)

#####################
img = np.asarray(cv2.imread('/Users/Domo3/Desktop/MAESTRIA 2019/SEGUNDO CUATRIMESTRE/deni/flor.jpg', 0))
gray=cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)