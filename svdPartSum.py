# -*- coding: utf-8 -*-
"""
Spyder Editor

@author: Andrea
This is a temporary script file.
"""
import numpy as np
import matplotlib.pyplot as plt

def svdPartSum(A,K):
  A=np.double(A)
  u,s,v=np.linalg.svd(A)
  AK = u[:,1:K] * s[1:K,1:K] * v[:,1:K].T
  AK=np.uint8(AK)
  plt.imshow(AK)
  return AK

def ComputMse(A, AK):
    m=np.size(A,1)
    n=np.size(A,2)
    e=0.0
    MSEA = 0.0
    A=np.double(A)
    AK=np.double(AK)
    for i in range (1,m):
        for j in range (1,n):
            e = A[i,j] - AK[i,j] ** 2
            MSEA =MSEA +e
    MSE=MSEA/(m*n) 
    return MSE,MSEA,m,n

