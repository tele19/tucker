#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 10 19:15:20 2020

@author: Domo3
"""
import numpy as np
def n_mode_unfold(X,mode):
    dim = X.shape
    dim_mode = dim[mode-1]
    order=len(dim)
    dim2 = np.asarray(dim)
    dim2[mode-1] = 1
    product = np.prod(dim2)
    if mode>order:
        print('error: mode must be lower than order')
    else:
        X_matrix = np.zeros((dim[mode-1],product))
        X_r = np.moveaxis(X,(mode-1),0)
        vec = np.reshape(X_r,(1,np.prod(dim)))
        c=0
        for i in range(dim_mode):
            for j in range(product):
                X_matrix[i,j]=vec[0,c]
                c=c+1
    return X_matrix
                
                