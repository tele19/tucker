#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb  8 17:40:20 2020

@author: domo3
"""
import tensorly as tl
import numpy as np
X=np.random.randn(5,4,3)
X1=tl.base.unfold(X, 0)
X2=tl.base.unfold(X, 1)
X3=tl.base.unfold(X, 2)
U1=np.random.randn(8,5)
Y1_=tl.tenalg.mode_dot(X,U1,0)
U2 = np.random.randn(9,4)
Y2_=tl.tenalg.mode_dot(X,U2,1)
U3=np.random.randn(6,3)
Y3_=tl.tenalg.mode_dot(X,U3,2)
dim=X1.shape
x1=np.reshape(X1,(1,dim[0]*dim[1]*dim[2]]))              
x2=np.reshape(X1,(1,dim[0]*dim[1]),'F')
dim2=X2.shape
x1_2=np.reshape(X2,(1,dim2[0]*dim2[1]))              
x2_2=np.reshape(X2,(1,dim2[0]*dim2[1]),'F')
dim3=X3.shape
x1_3=np.reshape(X3,(1,dim3[0]*dim3[1]))              
x2_3=np.reshape(X3,(1,dim3[0]*dim3[1]),'F')
