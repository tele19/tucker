# -*- coding: utf-8 -*-
"""
Created on Wed Feb  5 23:23:15 2020

@author: Andrea 
"""

import numpy as np
import matplotlib.pyplot as plt
import cv2


img=np.asarray(cv2.imread(r'/Users/Domo3/Desktop/MAESTRIA 2019/SEGUNDO CUATRIMESTRE/deni/flor2.jpg',0))#carga imagen convierte escala a grises
img2=np.asarray(cv2.imread(r'/Users/Domo3/Desktop/MAESTRIA 2019/SEGUNDO CUATRIMESTRE/deni/flor.jpg',0))#carga imagen convierte escala a grises

#muestra imagen escala a grises originales 
plt.figure(1)
plt.imshow(img,'gray')
plt.figure(2)
plt.imshow(img2,'gray')

########  obtención de la svd  ###########
h=300
w=300
dim=(w,h);

img= cv2.resize(img,dim);
img2= cv2.resize(img2,dim);
#combinación 1
u, s, vh=np.linalg.svd(img, full_matrices=False) 
u1, s1, vh1=np.linalg.svd(img2, full_matrices=False) 
combinf1=np.uint8(u*s*vh.T)
plt.figure(3)
plt.imshow(combinf1,'gray')
plt.title("combination of u1*s1*v1")

#combinación 2
combinf2=np.uint8(u*s*vh1.T) 
plt.figure(4) 
plt.imshow(combinf2,'gray')
plt.title("combination of u1*s1*v2") 

#combinación 3
combinf3=np.uint8(u1*s*vh.T)
plt.figure(5)
plt.imshow(combinf3,'gray')
plt.title("combination of u2*s1*v1") 

#combinación 4
combinf4=np.uint8(u1*s*vh1.T)
plt.figure(6)
plt.imshow(combinf4,'gray')
plt.title("combination of u2*s1*v2")

#combinación 5
combinf5=np.uint8(u*s1*vh1.T)
plt.figure(7)
plt.imshow(combinf5,'gray')
plt.title("combination of u1*s2*v1")

#combinación 6
combinf6=np.uint8(u*s1*vh1.T)
plt.figure(8)
plt.imshow(combinf6,'gray')
plt.title("combination of u1*s2*v2")
#combinación 7              
combinf7=np.uint8(u1*s1*vh1.T)
plt.figure(9)
plt.imshow(combinf7,'gray')
plt.title("combination of u2*s2*v1")

#combinación 8
combinf8=np.uint8(u1*s1*vh1.T)
plt.figure(10)
plt.imshow(combinf8,'gray')
plt.title("combination of u2*s2*v2")



