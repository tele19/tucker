#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 10 22:13:13 2020

@author: Domo3
"""

import numpy as np
import tensor_functions as te

X = np.random.randint(0,20,(5,4,3))
mode=1
X_matrix = te.n_mode_unfold(X,mode)
#%%
vec=te.vectorization(X,mode)
#%%
X = np.random.randint(0,20,(5,4,3))
U = np.random.randint(0,20,(2,5))
mode=1
Y=te.n_mode_product(X,U,mode)