#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  3 22:25:23 2020

@author: Andrea
"""

import numpy as np
from PIL import Image

Filename=open("trainingset.txt")
num=25
fid=open(Filename)
images=np.zeros((10304,num))
for i in range (1,num):
    face = fid.readline().rstrip()
    readface = np.asarray(Image.open(face))
    s=np.size(readface)
    ss=np.size(s)
    if ss[:,2] == 3:
        readface = rgb2gray(readface)   
    readface = np.resize(readface, (112,92))
    readface =(np.reshape(readface,(10304,1)))
    images[:,i] = readface
faces = images
S = np.size(faces,2)
psi = mean(faces,2)
for i in range (1,S):
    phi[:,i] = faces[:,i] - psi
u, sig, vt = np.linalg.svd(phi)
si = np.linalg.svd(sig)
tol = np.max(np.size(sig))* eps(np.max(si)) 
r = np.sum(si > tol)
row, col = np.size(sig)   
new_u =u[:,1:r]
#fclose(fid)
fid = open(Filename) 
images = np.zeros((10304,num))
for i in range(1,num):
    face = fid.readline().rstrip()
    readface = np.asarray(Image.open(face))
    s = np.size(readface)
    ss =np.size(s)
    if ss[:,2]== 3:
        readface = rgb2gray(readface)
    readface=np.resize(readface,[112,92]) 
    readface =np.double(np.reshape(readface,(10304,1))); 
    proj = new_u.T*(readface - psi)
    proj_matrix[:,i] = proj.T  
test = Image.open('test3.tif')
TestFace = np.resize(test,(112, 92))
TestFace = np.double(np.reshape(TestFace,(10304,1)));
ProjTestFace = new_u.T * (TestFace - psi)
for i in range (1,num):
    TestMatrix[:,i] = proj_matrix[:,i] - ProjTestFace
    DistanceMatrix[:,i] = np.sqrt(TestMatrix[:,i].T * TestMatrix[:,i])
row, col = np.size(DistanceMatrix)
for i in range (1,row):
    evalMatrix(1,i) = DistanceMatrix(i,i)
mi, index = np.min(evalMatrix) 
fid = open(Filename)
for i in range(1,index):
    face =fgetl(fid)
plt.figure(1)
plt.imshow(test)
plt.title("Input Image", fontsize = 10)
plt.figure(2) 
plt.imshow(face)
plt.title("Retrived Image", fontsize = 10)
i = range(1, np.size(evalMatrix,2))
plt.figure(3)
plt.stem(i,evalMatrix)
        
def getfaces(Filename, num):
    fid = open(Filename)
    images = zeros(10304, num)
    for i in range(1,num):
        face = fid.readline().rstrip()
        readface = np.asarray(Image.open(face))
        plt.subplot(np.ceil(np.sqrt(num)),np.ceil(np.sqrt(num)),1)
        print(1,'{}.\n'.format(face))
        readface = (np.reshape(readface,(10304,1)))
        figure = readface
face = images
readface = np.double(np.reshape(readface,(10304,1)))
              

     
def rgb2gray(img):
    grayImage = np.zeros(img.shape)
    R = np.array(img[:, :, 0])
    G = np.array(img[:, :, 1])
    B = np.array(img[:, :, 2])

    R = (R *.299)
    G = (G *.587)
    B = (B *.114)

    Avg = (R+G+B)
    grayImage = img

    for i in range(3):
         grayImage[:,:,i] = Avg

    return grayImage
