#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 10 19:15:20 2020

@author: Domo3
"""
import numpy as np

def n_mode_unfold(X,mode):
    dim = X.shape    
    order=len(dim)
    X_matrix=0    
    if mode>order:
        print('error: mode must be lower than order')
    else:
        dim_mode = dim[mode-1]
        dim2 = np.asarray(dim)
        dim2[mode-1] = 1
        product = np.prod(dim2)
        X_matrix = np.zeros((dim[mode-1],product))
        X_r = np.moveaxis(X,(mode-1),0)
        vec = np.reshape(X_r,(1,np.prod(dim)))
        c=0
        for i in range(dim_mode):
            for j in range(product):
                X_matrix[i,j]=vec[0,c]
                c=c+1
    return X_matrix
                

def vectorization(X,mode):
     dim = X.shape
     X_r = np.moveaxis(X,(mode-1),0)
     vec = np.reshape(X_r,(1,np.prod(dim)))
     return vec


def n_mode_product(X,U,mode):
    dim = X.shape 
    dim_U= U.shape
    order=len(dim)
    product=0    
    if mode>order:
        print('error: mode must be lower than order')
    elif dim[mode-1]!=dim_U[1]:
        print('error: matrix dimensions must match with the tensor dimensions in n mode')
    else:
        dim2=list(dim)
        dim2.remove(dim[mode-1])
        dim_tensor=[]
        dim_tensor.extend([dim_U[0]])
        dim_tensor.extend(dim2)
        product = np.zeros((dim_tensor))
        dim3 = np.asarray(dim_tensor)
        dim3[0] = 1
        product_dim = np.prod(dim3)
        product_vec=np.reshape(product,(dim_tensor[0],product_dim))
        X_r = np.moveaxis(X,(mode-1),0)
        vec = np.reshape(X_r,(1,np.prod(dim)))
        vec_U = np.reshape(U,(1,np.prod(dim_U)))
        for i in range(product_dim):
            c=0
            for j in range(dim_U[0]):
                v=vec[0,i*dim[mode-1]:i*dim[mode-1]+dim[mode-1]]
                w=vec_U[0,c:c+dim[mode-1]]
                product_vec[j,i]=np.dot(w,v)
                c = c + dim[mode-1]   
        product=np.reshape(product_vec,dim_tensor)
    return product    
    
def matrix_to_tensor(X,mode):
    return X

def matrix_multiplication(X,Y,type):
    
    return X
    

def svd_deni(A):
    A_T=A.T
    W=np.dot(A,A_T)
    
    return Y    
    
    
    