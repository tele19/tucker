#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 17 19:59:52 2020

@author: Domo3
"""

import numpy as np
import tensorly as tl

X = np.random.randint(0,20,(5,4,3))
U = np.random.randint(0,20,(2,4))
mode=2
dim = X.shape 
dim_U= U.shape
order=len(dim)
product=0    
if mode>order:
    print('error: mode must be lower than order')
elif dim[mode-1]!=dim_U[1]:
    print('error: matrix dimensions must match with the tensor dimensions in n mode')
else:
    dim2=list(dim)
    dim2[mode-1]=dim_U[0]
    dim_tensor=dim2
    product = np.zeros((dim_tensor))
    dim3 = np.asarray(dim_tensor)
    dim3[mode-1] = 1
    product_dim = np.prod(dim3)
    product_vec=np.reshape(product,(dim_tensor[mode-1],product_dim))
    X_r = np.moveaxis(X,(mode-1),-1)
    vec = np.reshape(X_r,(1,np.prod(dim)))
    vec_U = np.reshape(U,(1,np.prod(dim_U)))
    c=0
    for i in range(product_dim):
        for j in range(dim_tensor[mode-1]):
            v=vec[0,i*dim[mode-1]:i*dim[mode-1]+dim[mode-1]]
            w=vec_U[0,c:c+dim[mode-1]]
            product_vec[j,i]=np.dot(w,v)
            c = c + dim[mode-1]
            if c==np.prod(dim_U):
                c=0
    product=np.reshape(product_vec,dim_tensor)
Y1 = tl.tenalg.mode_dot(X,U,mode-1)
print('Product',product)
print('-----------------------------')
print('Y1',Y1)