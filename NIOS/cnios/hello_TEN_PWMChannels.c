#include <sys/alt_stdio.h>
#include <stdio.h>
#include "altera_avalon_pio_regs.h"
#include "system.h"

int main()
{
	int switch_datain;
	int counter=0;
	int counters[10];
	int ii;
	int delay;
	alt_putstr("Hello from NiosII with Ten PWM Channels!\n");
	alt_putstr("Lets give PWM signaling to LEDS\n");
	/* Event loop never exits. Read the PB, display on the LED */
	counters[0]=0;
	counters[1]=1562;
	counters[2]=2*1562;
	counters[3]=3*1562;
	counters[4]=4*1562;
	counters[5]=5*1562;
	counters[6]=6*1562;
	counters[7]=7*1562;
	counters[8]=8*1562;
	counters[9]=9*1562;
	while (1)
	{
		delay = 10;
		//Gets the data from the pb, recall that a 0 means the button is pressed
		//switch_datain = ~IORD_ALTERA_AVALON_PIO_DATA(BUTTON_BASE);
		//Mask the bits so the leftmost LEDs are off (we only care about LED3-0)
		//switch_datain &= (0b0000000011);
		//Send the data to the LED
			IOWR_ALTERA_AVALON_PIO_DATA(REG32_AVALON_INTERFACE_0_BASE,counters[0]);
			usleep(delay);
			IOWR_ALTERA_AVALON_PIO_DATA(REG32_AVALON_INTERFACE_1_BASE,counters[1]);
			usleep(delay);
			IOWR_ALTERA_AVALON_PIO_DATA(REG32_AVALON_INTERFACE_2_BASE,counters[2]);
			usleep(delay);
			IOWR_ALTERA_AVALON_PIO_DATA(REG32_AVALON_INTERFACE_3_BASE,counters[3]);
			usleep(delay);
			IOWR_ALTERA_AVALON_PIO_DATA(REG32_AVALON_INTERFACE_4_BASE,counters[4]);
			usleep(delay);
			IOWR_ALTERA_AVALON_PIO_DATA(REG32_AVALON_INTERFACE_5_BASE,counters[5]);
			usleep(delay);
			IOWR_ALTERA_AVALON_PIO_DATA(REG32_AVALON_INTERFACE_6_BASE,counters[6]);
			usleep(delay);
			IOWR_ALTERA_AVALON_PIO_DATA(REG32_AVALON_INTERFACE_7_BASE,counters[7]);
			usleep(delay);
			IOWR_ALTERA_AVALON_PIO_DATA(REG32_AVALON_INTERFACE_8_BASE,counters[8]);
			usleep(delay);
			IOWR_ALTERA_AVALON_PIO_DATA(REG32_AVALON_INTERFACE_9_BASE,counters[9]);
			usleep(delay);
			//counter = 15624/8;
			if (counters[0] < 15624){
				counters[0] = counters[0]+16;
				}
			else
				counters[0] =0;

			if (counters[1] < 15624){
							counters[1] = counters[1]+16;
							}
						else
							counters[1] =0;

			if (counters[2] < 15624){
							counters[2] = counters[2]+16;
							}
						else
							counters[2] =0;
			if (counters[3] < 15624){
							counters[3] = counters[3]+16;
							}
						else
							counters[3] =0;
			if (counters[4] < 15624){
							counters[4] = counters[4]+16;
							}
						else
							counters[4] =0;
			if (counters[5] < 15624){
							counters[5] = counters[5]+16;
							}
						else
							counters[5] =0;
			if (counters[6] < 15624){
							counters[6] = counters[6]+16;
							}
						else
							counters[6] =0;
			if (counters[7] < 15624){
							counters[7] = counters[7]+16;
							}
						else
							counters[7] =0;
			if (counters[8] < 15624){
							counters[8] = counters[8]+16;
							}
						else
							counters[8] =0;
			if (counters[9] < 15624){
							counters[9] = counters[9]+16;
							}
						else
							counters[9] =0;



	}
	return 0;
}
