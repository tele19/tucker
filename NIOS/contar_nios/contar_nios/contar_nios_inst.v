	contar_nios u0 (
		.clk_clk                         (<connected-to-clk_clk>),                         //                      clk.clk
		.ia_external_connection_export   (<connected-to-ia_external_connection_export>),   //   ia_external_connection.export
		.ib_external_connection_export   (<connected-to-ib_external_connection_export>),   //   ib_external_connection.export
		.is_external_connection_export   (<connected-to-is_external_connection_export>),   //   is_external_connection.export
		.oled_external_connection_export (<connected-to-oled_external_connection_export>)  // oled_external_connection.export
	);

