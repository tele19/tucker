
module contar_nios (
	clk_clk,
	ia_external_connection_export,
	ib_external_connection_export,
	is_external_connection_export,
	oled_external_connection_export);	

	input		clk_clk;
	input	[3:0]	ia_external_connection_export;
	input	[3:0]	ib_external_connection_export;
	input	[1:0]	is_external_connection_export;
	output	[7:0]	oled_external_connection_export;
endmodule
