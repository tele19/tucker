	component contar_nios is
		port (
			clk_clk                         : in  std_logic                    := 'X';             -- clk
			ia_external_connection_export   : in  std_logic_vector(3 downto 0) := (others => 'X'); -- export
			ib_external_connection_export   : in  std_logic_vector(3 downto 0) := (others => 'X'); -- export
			is_external_connection_export   : in  std_logic_vector(1 downto 0) := (others => 'X'); -- export
			oled_external_connection_export : out std_logic_vector(7 downto 0)                     -- export
		);
	end component contar_nios;

	u0 : component contar_nios
		port map (
			clk_clk                         => CONNECTED_TO_clk_clk,                         --                      clk.clk
			ia_external_connection_export   => CONNECTED_TO_ia_external_connection_export,   --   ia_external_connection.export
			ib_external_connection_export   => CONNECTED_TO_ib_external_connection_export,   --   ib_external_connection.export
			is_external_connection_export   => CONNECTED_TO_is_external_connection_export,   --   is_external_connection.export
			oled_external_connection_export => CONNECTED_TO_oled_external_connection_export  -- oled_external_connection.export
		);

