module unsaved_inst(
	input  MAX10_CLK1_50,
	input	 [1:0]KEY,
	input  [9:0]SW,
	output [9:0]LEDR,
	output [7:0]HEX0,HEX1,HEX2,HEX3,HEX4,HEX5

);	

unsaved u0 (
		.clk_clk                           (MAX10_CLK1_50), //  clk.clk
		.reset_reset_n                     (1'b1),   //         reset.reset_n
		.hex5_external_connection_export   (HEX5),   //   hex5_external_connection.export
		.hex4_external_connection_export   (HEX4),   //   hex4_external_connection.export
		.hex3_external_connection_export   (HEX3),   //   hex3_external_connection.export
		.hex2_external_connection_export   (HEX2),   //   hex2_external_connection.export
		.hex0_external_connection_export   (HEX0),   //   hex0_external_connection.export
		.switch_external_connection_export (SW[9:0]), // switch_external_connection.export
		.led_external_connection_export    (LEDR[9:0]),// led_external_connection.export
		.button_external_connection_export (KEY[1:0]), // button_external_connection.export
		.hex1_external_connection_export   (HEX1)    //   hex1_external_connection.export
	);

assign LEDR[2]=SW[2];

wire [9:0] ledFromNios;
assign LEDR[1:0]=ledFromNios[1:0];
assign LEDR[9:3]=ledFromNios[9:3];
assign HEX0[7]=1'b1;
assign HEX1[7]=1'b1;
assign HEX2[7]=1'b1;
assign HEX3[7]=1'b1;
assign HEX4[7]=1'b1;
assign HEX5[7]=1'b1;
endmodule 
