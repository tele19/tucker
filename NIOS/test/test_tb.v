`timescale 1ns/100ps
module test_tb ();
reg  a;
reg  b;
wire c;
test dut (.a(a),.b(b),.c(c));
initial begin
a=1'b0;
b=1'b0;
#100;
a=1'b0;
b=1'b1;
#100;
a=1'b1;
b=1'b0;
#100;
a=1'b1;
b=1'b1;
#100;
a=1'b0;
b=1'b0;
#100;
end
endmodule
