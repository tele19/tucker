module test(
	input        CLK, RST,
	input	 [1:0] KEY,                          // PB   2
	input  [9:0] SW,                           // SW  10
	output [8:0] LEDR,                         // LED  9
	output [6:0] HEX0,HEX1,HEX2,HEX3,HEX4,HEX5 // HEX  6
);

	scnios u0 (
		.clk_clk                           (CLK),         //                        clk.clk
		.reset_reset_n                     (1'b1),         //                      reset.reset_n
		.button_external_connection_export (KEY),         // button_external_connection.export
		.led_external_connection_export    (ledFromNios), //    led_external_connection.export
		.switch_external_connection_export (SW),          // switch_external_connection.export
		.hex0_external_connection_export   (HEX0),        //   hex0_external_connection.export
		.hex1_external_connection_export   (HEX1),        //   hex1_external_connection.export
		.hex2_external_connection_export   (HEX2),        //   hex2_external_connection.export
		.hex3_external_connection_export   (HEX3),        //   hex3_external_connection.export
		.hex4_external_connection_export   (HEX4),        //   hex4_external_connection.export
		.hex5_external_connection_export   (HEX5)         //   hex5_external_connection.export
	);

wire [8:0] ledFromNios;

assign LEDR[1:0] = ledFromNios[1:0];
assign LEDR[2]   = SW[2];
assign LEDR[8:3] = ledFromNios[8:3];

endmodule
