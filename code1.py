# -*- coding: utf-8 -*-
"""
Created on Mon Feb  3 20:48:13 2020

@author: Andrea
"""
import numpy as np
from PIL import Image
import matplotlib.pyplot as plt

def svdRecognition0(newName, r, N, A, U, S, V, fbar, e0, e1):
    Ur = U[:, 1:r]
    X = np.dot(Ur.T,A)
    fnew = np.asarray(Image.open(newName))
    fnew = np.resize(fnew, (112,92))
    f = np.reshape(fnew, (10304,1))
    f0 = np.double(f) - fbar
    x = Ur.T*f0
    fp = Ur*x
    dif = f0 - fp
    ef = np.linalg.norm(dif)
    if (ef < e1):
        D = X - x * np.ones((1, N))
        d = np.sqrt(np.diag(D.T*D))
        dmin = np.min(d)
        indx = np.argmin(d)
        if (dmin < e0):
            print('This image is face {0}'.format(indx))
        else:
            print('The input image is an unknown face')
    else:
        print('The input image is not a face')
    return ef, d

def svdDecomp(fileName, N):
#fileName = 'imageset.txt'
    fid = np.loadtxt(fileName)
    S = np.zeros((10304, N))
    for i in range(N):
        face = fid.readline(fid).rstrip()
        fi = Image.open(face)
        plt.subplot(np.ceil(np.sqrt(N)), np.ceil(np.sqrt(N)), i)
        print(1,'{}.\n'.format(face))
        plt.figure(1)
        plt.imshow(fi)
        fi = np.double(np.reshape(fi,(10304,1)))
        S[:,i] = fi
    fbar = (np.mean(S.T)).T
    plt.figure(2)
    plt.imshow(np.reshape(np.uint8(fbar), (112, 92)))
    A = S - fbar * np.ones((1, N))
    U, S, V = np.linalg.svd(A)
    
        