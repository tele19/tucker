#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  3 21:21:50 2020

@author: Andrea
"""

import numpy as np
from PIL import Image
import matplotlib.pyplot as plt
import matplotlib.image as mpimg

def SVDExchange(Image1,Image2):
  A1=np.resize(Image.open(Image1),(112,92))  
  A2=np.resize(Image.open(Image2),(112,92))
  s1=np.size(A1)
  s2=np.size(A2)
  ss1=np.size(s1)
  ss2=np.size(s2)
  
  if (ss1[:,2]==3):
      A1 = rgb2gray(A1)

  
  if (ss2[:,2]==3):
      A2 = rgb2gray(A1)
  
  u1,s1,v1=np.linalg.svd(np.double(A1)) 
  u2,s2,v2=np.linalg.svd(np.double(A2))
  combinf1=np.uint8(u1*s1*v1.T)
  plt.figure(1) 
  plt.imshow(combinf1)
  plt.title("combination of u1*s1*v1") 
  combinf2=np.uint8(u1*s1*v2.T) 
  plt.figure(2) 
  plt.imshow(combinf2)
  plt.title("combination of u1*s1*v2") 
  combinf3=np.uint8(u2*s1*v1.T)
  plt.figure(3)
  plt.imshow(combinf3)
  plt.title("combination of u2*s1*v1") 
  combinf4=np.uint8(u2*s1*v2.T)
  plt.figure(4)
  plt.imshow(combinf4)
  plt.title("combination of u2*s1*v2")
  combinf5=np.uint8(u1*s2*v1.T)
  plt.figure(5)
  plt.imshow(combinf5)
  plt.title("combination of u1*s2*v1")
  combinf6=np.uint8(u1*s2*v2.T)
  plt.figure(6)
  plt.imshow(combinf6)
  plt.title("combination of u1*s2*v2")              
  combinf7=np.uint8(u2*s2*v1.T)
  plt.figure(7)
  plt.imshow(combinf7)
  plt.title("combination of u2*s2*v1")
  combinf8=np.uint8(u2*s2*v2.T)
  plt.figure(8)
  plt.imshow(combinf8)
  plt.title("combination of u2*s2*v2")
  return A1,A2

def rgb2gray(img):
    grayImage = np.zeros(img.shape)
    R = np.array(img[:, :, 0])
    G = np.array(img[:, :, 1])
    B = np.array(img[:, :, 2])

    R = (R *.299)
    G = (G *.587)
    B = (B *.114)

    Avg = (R+G+B)
    grayImage = img

    for i in range(3):
         grayImage[:,:,i] = Avg

    return grayImage
