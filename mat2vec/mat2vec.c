#include <stdio.h>

void printMat(int *, int, int);
void mat2vec(int *, int *, int, int);
void printVec(int *, int);

/******************************************************************************/
int main() {
	int m = 4, n = 3;
	int matriz[m][n] = {{1,2,3},{2,3,1},{3,1,2},{4,5,6}};
	int vector[m*n];
	printMat((int *) matriz, m, n);
	mat2vec((int *) matriz, (int *) vector, m, n);
	printVec((int *) vector, m*n);

	printf("Hello World!\n");
	return 0;
}

/******************************************************************************/
void printMat(int * A, int m, int n) {
	printf("[");
	for (int i = 0; i < m; i++) {
		printf("[");
		for (int j = 0; j < n; j++) {
			printf("%d", *(A+i*n+j));
			if (j + 1 < n) printf(" ");
		}
		printf("]");
		if (i + 1 < m) printf("\n");
	}
	printf("]\n");
}

/******************************************************************************/
void mat2vec(int * A, int * a, int m, int n) {
	for (int i = 0; i < m; i++) {
		for (int j = 0; j < n; j++) {
			*a = *(A+i*n+j);
		}
	}
}

/******************************************************************************/
void printVec(int * a, int mn) {
	printf("[");
	for (int i = 0; i < mn; i++) {
		printf("%d", *(a+i));
		if (i + 1 < mn) printf(" ");
	}
	printf("]\n");
}
